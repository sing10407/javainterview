package StackAndQueue;


import java.util.Scanner;
import java.util.Stack;

// https://www.hackerrank.com/challenges/ctci-queue-using-two-stacks
public class Queues_TaleOfTwoStacks {
    public static class MyQueue<T> {
        Stack<T> stackNewestOnTop = new Stack<T>();
        Stack<T> stackOldestOnTop = new Stack<T>();

        public void enqueue(T value) { // Push onto newest stack
            stackNewestOnTop.push(value);
        }

        public T peek() {
            reverseStack();
            T value = stackOldestOnTop.peek();
            return value;
        }

        public T dequeue() {
            reverseStack();
            T value = stackOldestOnTop.pop();
            return value;
        }

        // perform stack transfers only when stackOldestOnTop.isEmpty()
        // increasing performance
        private void reverseStack(){
            if(stackOldestOnTop.isEmpty()){
                while(!stackNewestOnTop.isEmpty()){
                    T value = stackNewestOnTop.pop();
                    stackOldestOnTop.push(value);
                }
            }
        }
    }


    public static void main(String[] args) {
        MyQueue<Integer> queue = new MyQueue<Integer>();

        Scanner scan = new Scanner(System.in);
        int n = scan.nextInt();

        for (int i = 0; i < n; i++) {
            int operation = scan.nextInt();
            if (operation == 1) { // enqueue
                queue.enqueue(scan.nextInt());
            } else if (operation == 2) { // dequeue
                queue.dequeue();
            } else if (operation == 3) { // print/peek
                System.out.println(queue.peek());
            }
        }
        scan.close();
    }
}
