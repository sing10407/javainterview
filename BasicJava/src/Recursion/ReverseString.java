package Recursion;

import java.util.Stack;

public class ReverseString {
    public static void main(String[] args) {
        ReverseString reverseString = new ReverseString();
        System.out.println(reverseString.reverse("test"));
        System.out.println(reverseString.reverse("abcde"));
        System.out.println(reverseString.reverseIteratively("abcde"));
    }
    public String reverse(String s){

        if(s.length() == 0)
            return "";

        String head = s.substring(0, 1);
        return reverse(s.substring(1, s.length())) + head;
    }
    public String reverseIteratively(String s){

        Stack<Character> stack = new Stack<Character>();
        for(int i = 0 ; i < s.length() ; i ++)
            stack.push(s.charAt(i));

        String result = "";
        while (stack.size() > 0)
            result = result + stack.pop();

        return result;
    }
}
