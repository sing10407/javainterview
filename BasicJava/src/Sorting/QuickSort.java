package Sorting;

public class QuickSort {
    public static void main(String[] args) {
        int[] arr = new int[]{3,55,1,44,33,2};
        arr = sortAsc(arr, 0, arr.length - 1);
        for(int i : arr)
            System.out.println(i);
    }
    public static int[] sortAsc(int[] arr, int left, int right){

        if(left >= right)
            return arr;

        int pivot = arr[left];

        int tempLeft = left;
        int tempRight = right;
        while(tempLeft < tempRight){
            while (arr[tempLeft] < pivot && tempLeft < tempRight)
                tempLeft ++;
            while (arr[tempRight] > pivot && tempLeft < tempRight)
                tempRight --;

            if(tempLeft > tempRight)
                break;

            int tmp = arr[tempLeft];
            arr[tempLeft] = arr[tempRight];
            arr[tempRight] = tmp;
        }

        sortAsc(arr, left, tempRight - 1);
        sortAsc(arr, tempRight + 1, right);
        return arr;
    }
}
