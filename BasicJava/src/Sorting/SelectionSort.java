package Sorting;

public class SelectionSort {
    public static void main(String[] args) {
        for(int i :sortAsc(new int[]{566,3,11,33,4}))
            System.out.println(i);
    }
    public static int[] sortAsc(int[] arr){

        for(int i = 0 ; i < arr.length ; i ++){
            int maxIndex = 0;
            for(int j = 0 ; j < arr.length - i ; j ++){
                if(arr[j] > arr[maxIndex])
                    maxIndex = j;
            }

            int tmp = arr[maxIndex];
            arr[maxIndex] = arr[arr.length - i - 1];
            arr[arr.length - i - 1] = tmp;
        }
        return arr;
    }
}
