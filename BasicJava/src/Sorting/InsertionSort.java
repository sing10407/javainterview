package Sorting;

public class InsertionSort {
    public static void main(String[] args) {
        int[] result = sortAsc(new int[]{1,5,3,55,3,6,33,4});
        for(int i = 0 ; i < result.length ; i ++)
            System.out.println(result[i]);
    }
    public static int[] sortAsc(int[] arr){

        for(int i = 0 ; i < arr.length ; i ++){
            int temp = arr[i];
            for(int j = i ; j > 0 ; j--){
                if(arr[j - 1] < temp){
                    arr[j] = temp;
                    break;
                }else{
                    arr[j] = arr[j - 1];
                }
            }
        }
        return arr;
    }
}
