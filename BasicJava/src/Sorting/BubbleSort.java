package Sorting;

public class BubbleSort {
    public static void main(String[] args) {
        for(int i :sortAsc(new int[]{566,3,11,33,4}))
            System.out.println(i);
    }
    public static int[] sortAsc(int[] arr){

        for(int i = 0 ; i < arr.length ; i ++){
            for(int j = 1; j < arr.length - i ; j ++){
                if(arr[j] < arr[j - 1]){
                    int tmp = arr[j];
                    arr[j] = arr[j - 1];
                    arr[j - 1] = tmp;
                }
            }
        }
        return arr;
    }
}
