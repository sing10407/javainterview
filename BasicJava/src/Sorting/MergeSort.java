package Sorting;

public class MergeSort {
    public static void main(String[] args) {
        int[] arr = new int[]{3,5,1,6,2,33,4,55,66};
        MergeSort mergeSort = new MergeSort();
        int[] result = mergeSort.mergeSort(arr, 0, arr.length -1);

        for(int i : result)
            System.out.println(i);

    }
    public int[] mergeSort(int[] arr, int start, int end){

        if(end<=1)
            return arr;

        int mid = (start+end)/2;
        int[] left = new int[mid-start];
        int[] right =  new int[end-mid+1];
        for(int i=start;i<mid;i++){
            left[i]=arr[i];
        }
        for(int i=mid;i<=end;i++){
            right[i-mid]=arr[i];
        }
        left = mergeSort(left,0, left.length-1);
        right = mergeSort(right, 0, right.length-1);
        return merge(left, right);
    }

    private int[] merge(int[] a, int[] b){
        int[] result = new int[a.length+b.length];

        int aIndex=0, bIndex=0, i=0;
        while(aIndex<a.length && bIndex<b.length){
            if(a[aIndex]>b[bIndex]){
                result[i]=b[bIndex];
                bIndex++;
            }else{
                result[i]=a[aIndex];
                aIndex++;
            }
            i++;
        }
        while (aIndex<a.length){
            result[i]=a[aIndex];
            i++;
            aIndex++;
        }
        while(bIndex<b.length){
            result[i]=b[bIndex];
            i++;
            bIndex++;
        }
        return result;
    }

}
