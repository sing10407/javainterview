package Graph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Node {
    private String name;
    private List<Edge> neighbors;

    public Node(String name) {
        this.name = name;
        neighbors = new ArrayList<Edge>();
    }

    public void AddUndirectedEdge(Node destination, int weight) {
        neighbors.add(new Edge(destination, weight));
        destination.neighbors.add(new Edge(this, weight));
    }

    private static class Edge {
        public Node destination;
        public int weight;

        public Edge(Node destination, int weight) {
            this.destination = destination;
            this.weight = weight;
        }
    }

    public static void main(String[] args) {
        // can find certain node by key
        HashMap<String, Node> graph = new HashMap<String, Node>();
    }
}