package Graph;


// https://www.hackerrank.com/challenges/ctci-connected-cell-in-a-grid/problem?h_l=interview&playlist_slugs%5B%5D=interview-preparation-kit&playlist_slugs%5B%5D=graphs
public class DFS_ConnectedCellInGrid {

    // Complete the maxRegion function below.
    static int maxRegion(int[][] grid) {
        boolean[][] reached = new boolean[grid.length][grid[0].length];
        int max = Integer.MIN_VALUE;
        for(int i = 0 ; i < grid.length ; i ++){
            for(int j = 0 ; j < grid[0].length ; j ++){
                if(reached[i][j])
                    continue;
                if(grid[i][j] == 1){
                    int temp = getMaxAreas(reached, grid, i, j);
                    if(temp > max)
                        max = temp;
                }

            }
        }
        return max;

    }

    static int getMaxAreas(boolean[][] reached, int[][] grid, int i, int j){
        int maxColumn = grid[0].length - 1;
        int maxRow = grid.length - 1;

        if(reached[i][j])
            return 0;
        else
            reached[i][j] = true;

        int result = 1;

        if(j + 1 <= maxColumn && grid[i][j + 1] == 1){
            result += getMaxAreas(reached, grid, i, j + 1);
        }
        if(i + 1 <= maxRow && grid[i + 1][j] == 1){
            result += getMaxAreas(reached, grid, i + 1, j);
        }
        if(i + 1 <= maxRow && j + 1 <= maxColumn && grid[i + 1][j + 1] == 1){
            result += getMaxAreas(reached, grid, i + 1, j + 1);
        }
        if(i > 0 && i - 1 <= maxRow && j + 1 <= maxColumn && grid[i - 1][j + 1] == 1){
            result += getMaxAreas(reached, grid, i - 1, j + 1);
        }
        if(j > 0 && i + 1 <= maxRow && j - 1 <= maxColumn && grid[i + 1][j - 1] == 1){
            result += getMaxAreas(reached, grid, i + 1, j - 1);
        }
        if(j > 0 && grid[i][j - 1] == 1){
            result += getMaxAreas(reached, grid, i, j - 1);
        }
        if(i > 0 && grid[i - 1][j] == 1){
            result += getMaxAreas(reached, grid, i - 1, j);
        }
        if(i > 0 && j > 0 && grid[i - 1][j - 1] == 1){
            result += getMaxAreas(reached, grid, i - 1, j - 1);
        }
        return result;
    }

}
