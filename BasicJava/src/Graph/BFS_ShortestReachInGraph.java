package Graph;

import java.util.*;

public class BFS_ShortestReachInGraph {


    public static class Node {
        int key;
        List<Edge> adjs;
        public Node(int k, List<Edge> adjs){
            key = k;
            this.adjs = adjs;
        }
    }

    public static class Edge {
        int weight;
        int key;
        public Edge(int w, int key){
            weight = w;
            this.key = key;
        }
    }

    public static class Graph {

        int graphSize;
        Hashtable<Integer, Node> graph = new Hashtable<Integer, Node>();

        public Graph(int size) {
            graphSize = size;

            for(int i=1;i<=graphSize;i++){
                Node temp = new Node(i,  new ArrayList<Edge>());
                graph.put(i, temp);
            }
        }

        public void addEdge(int first, int second) {
            first = first + 1;
            second = second + 1;
            Node firstNode = graph.get(first);
            Node secondNode = graph.get(second);
            firstNode.adjs.add(new Edge(6, second));
            secondNode.adjs.add(new Edge(6, first));
            graph.put(first, firstNode);
            graph.put(second, secondNode);
        }

        public int[] shortestReach(int startId) { // 0 indexed
            startId = startId+1;

            int[] result = new int[graphSize];
            Arrays.fill(result, -1);

            Queue<Edge> queue = new LinkedList<Edge>();
            HashSet<Integer> seen = new HashSet<Integer>();
            int w=0;
            Node start = graph.get(startId);
            seen.add(start.key);
            for(int j=0;j<start.adjs.size();j++){
                Edge childEdge = start.adjs.get(j);
                queue.add(childEdge);
                seen.add(childEdge.key);
                result[childEdge.key - 1] = 6;
            }


            while(!queue.isEmpty()){

                Edge childEdge = queue.remove();
                Node child = graph.get(childEdge.key);

                for(int k=0;k<child.adjs.size();k++){
                    Edge childChildEdge = child.adjs.get(k);
                    if(!seen.contains(childChildEdge.key)){
                        queue.add(childChildEdge);
                        seen.add(childChildEdge.key);
                        result[childChildEdge.key - 1] = result[childEdge.key - 1] + childChildEdge.weight;
                    }
                }
            }
            return result;
        }
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int queries = scanner.nextInt();

        for (int t = 0; t < queries; t++) {

            // Create a graph of size n where each edge weight is 6:
            Graph graph = new Graph(scanner.nextInt());
            int m = scanner.nextInt();

            // read and set edges
            for (int i = 0; i < m; i++) {
                int u = scanner.nextInt() - 1;
                int v = scanner.nextInt() - 1;

                // add each edge to the graph
                graph.addEdge(u, v);
            }

            // Find shortest reach from node s
            int startId = scanner.nextInt() - 1;
            int[] distances = graph.shortestReach(startId);

            for (int i = 0; i < distances.length; i++) {
                if (i != startId) {
                    System.out.print(distances[i]);
                    System.out.print(" ");
                }
            }
            System.out.println();
        }

        scanner.close();
    }
}
