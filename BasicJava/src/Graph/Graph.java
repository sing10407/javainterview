package Graph;

import java.util.*;

public class Graph {

    public static class Node{
        private int id;
        LinkedList<Node> adjacent = new LinkedList<Node>(); // children
        private Node(int id){
            this.id = id;
        }
    }

    private Node getNode(int id){
        // todo
        return new Node(1);
    }

    public void addEdge(int source, int destination){
        Node s = getNode(source);
        Node d = getNode(destination);
        s.adjacent.add(d);
    }

    public boolean hasPathDFS(int source, int destination){
        Node s = getNode(source);
        Node d = getNode(destination);
        HashSet<Integer> visited = new HashSet<Integer>();
        return hasPathDFS(s, d, visited);
    }

    private boolean hasPathDFS(Node s, Node d, HashSet<Integer> visited) {
        if(visited.contains(s))
            return false;
        visited.add(s.id);
        // target found !
        if(s.id == d.id)
            return true;

        for(Node child : s.adjacent){
            if(hasPathDFS(child, d, visited))
                return true;
        }
        return false;
    }

    private boolean hasPathDfsStack(Node s, Node d, HashSet<Integer> visited) {
        Stack<Node> stack = new Stack<>();
        stack.push(s);
        visited.add(s.id);

        while (!stack.empty()){
            Node n = stack.pop();
            for(Node child : n.adjacent){
                if(child.id == d.id)
                    return true;
                if(!visited.contains(s)){
                    stack.push(child);
                    visited.add(child.id);
                }
            }
        }
        return false;
    }

    private boolean hasPathBFS(Node s, Node d) {
        Queue<Node> queue = new LinkedList<Node>();

        queue.add(s);

        while(!queue.isEmpty()){
            Node current = queue.remove();

            if(current.id == d.id)
                return true;

            for (Node child : current.adjacent)
                queue.add(child);

        }
        return false;
    }
}
