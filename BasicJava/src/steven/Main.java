package steven;

import LinkedList.ReverseLinkedList;

import java.math.BigInteger;
import java.util.*;

public class Main {

    public static void main(String[] args) {

        // string
        String text = "ggg12345";
        for(char ch : text.toCharArray())
            System.out.println("toCharArray :"+ch);
        for(int i = 0; i < text.length(); i++)
            System.out.println("charAt :"+text.charAt(i));

        // BigInteger
        BigInteger bi1 = new BigInteger("100");
        BigInteger bi2 = new BigInteger("200");
        System.out.println(bi1.multiply(bi2));
        System.out.println(bi1.subtract(bi2));
        System.out.println(bi1.add(bi2));
        System.out.println(bi1.divide(bi2));
        System.out.println(bi1.abs());

        // hash table
        Hashtable<String,Integer> ht = new Hashtable<String,Integer>();
        ht.put("gg",123);
        ht.put("gg",456);
        System.out.println(ht.containsKey("gg"));
        System.out.println(ht.get("gg"));
        System.out.println(ht.size()); // size

        // hashset
        HashSet<String> reached = new HashSet<String>();
        reached.size(); // size
        if(reached.contains("test")){

        }else{
            reached.add("test");
        }

        // array
        String[] myStringArray = new String[3];
        myStringArray = new String[]{"a", "b", "c"};
        for(int i=0;i<myStringArray.length;i++)
            System.out.println(myStringArray[i]);

        int[][] myInt2dArray = new int[][]{
                {12,22,33},
                {44,55,66}
        };

        // sort array
        Integer[] arr = new Integer[]{1,5,3};
        Arrays.sort(arr);
        Arrays.sort(arr, Collections.reverseOrder());

        // clone array
        int[] a = new int[]{1,2,3,4,5};
        int[] b = a.clone();

        // 2d array
        for(int i=0;i<myInt2dArray.length;i++)
            for(int j=0;j<myInt2dArray[i].length;j++)
                System.out.println(myInt2dArray[i][j]);



        // int border value
        int maxInt=Integer.MAX_VALUE;
        int minInt=Integer.MIN_VALUE;

        // substring
        // beg inIndex -- from 0, contains.
        // end index -- not contains.
        String Str ="www.runoob.com";
        System.out.print("test:" );
        System.out.println(Str.substring(4, 10));

        // reverse a string
        String rev_s = new StringBuilder("google").reverse().toString();
        System.out.println(rev_s);


        // stack
        Stack<String> stack = new Stack<String>();
        stack.push("TEST");
        if(!stack.empty())
            System.out.println(stack.pop());

        // queue
        Queue<String> queue = new LinkedList<String>();
        queue.add("aaa");
        String aa = queue.remove();
        queue.isEmpty();
        queue.peek(); // get first element, but not delete it

        //remove duplicate
        int[] dupArr = new int[]{1, 2, 2, 3, 3, 3};
        int[] newArr = Arrays.stream(dupArr).distinct().toArray();
        for(int i : newArr)
            System.out.println(i);

        // long int multiply
        int i = 1234567890;
        int j = 1234567890;
        long longMultiply = i * j;
        // wrong answer
        System.out.println(longMultiply);
        // correct answer
        long longMultiply2 = (long)i * (long)j;
        System.out.println(longMultiply2);

        // switch
        char c = '{';
        switch(c){
            case '(':
            case '[':
            case '{':
                System.out.println("hi");
                break;
            default:
                System.out.println("hello");
        }

        // string compare
        String s1 = new String("hello");
        String s2 = new String("hello");
        if(s2 == s1)
            System.out.println("hello == hello");
        else
            System.out.println("hello != hello");
        if(s1.equals(s2))
            System.out.println("hello.equals(hello)");


        // string compare
        String aaa = "...";
        String bbb = "...";
        int compare = aaa.compareTo(bbb);
        if (compare < 0) {
            //a is smaller
        }
        else if (compare > 0) {
            //a is larger
        }
        else {
            //a is equal to b
        }


    }
}
