package steven;

import java.util.*;

public class Test {

    public static class Node{
        int key;
        List<Node> adj;
    }

    public static class Graph {

        Hashtable<Integer, Node> _graph = new Hashtable<Integer, Node>();
        int _size = 0;

        public Graph(int size) {
            _size = size;
            for(int i = 1 ; i <= size ; i ++){
                Node newNode = new Node();
                newNode.key = i;
                newNode.adj = new ArrayList<Node>();
                _graph.put(i, newNode);
            }
        }

        public void addEdge(int first, int second) {
            Node nodeFirst = _graph.get(first + 1);
            Node nodeSecond = _graph.get(second + 1);

            nodeFirst.adj.add(nodeSecond);
            _graph.put(first + 1, nodeFirst);

            nodeSecond.adj.add(nodeFirst);
            _graph.put(second + 1, nodeSecond);
        }

        public int[] shortestReach(int startId) { // 0 indexed
            startId = startId + 1;

            int[] result = new int[_size];

            for(int i = 1 ; i <= _size ; i ++){
                if(i == startId)
                    continue;
                Node node = _graph.get(i);
                int depth = getDepth(startId, i);
                if(depth != -1)
                    result[i - 1] = depth * 6;
                else
                    result[i - 1] = depth;
            }
            return result;
        }
        private int getDepth(int start, int end){
            Queue<Node> queue = new LinkedList<Node>();
            Node startNode = _graph.get(start);
            int[] depthOfNodes = new int[_size + 1];

            List<Node> childs = startNode.adj;
            for(Node n : childs){
                queue.add(n);
            }
            while(!queue.isEmpty()){
                Node n = queue.remove();
                if(n.key == end){
                    return depthOfNodes[n.key] + 1;
                }
                for(Node tmpNode : n.adj){
                    if(tmpNode.key == start)
                        break;
                    depthOfNodes[tmpNode.key] = depthOfNodes[n.key] + 1;
                    queue.add(tmpNode);
                }
            }
            return -1;
        }

    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int queries = scanner.nextInt();

        for (int t = 0; t < queries; t++) {

            // Create a graph of size n where each edge weight is 6:
            Graph graph = new Graph(scanner.nextInt());
            int m = scanner.nextInt();

            // read and set edges
            for (int i = 0; i < m; i++) {
                int u = scanner.nextInt() - 1;
                int v = scanner.nextInt() - 1;

                // add each edge to the graph
                graph.addEdge(u, v);
            }

            // Find shortest reach from node s
            int startId = scanner.nextInt() - 1;
            int[] distances = graph.shortestReach(startId);

            for (int i = 0; i < distances.length; i++) {
                if (i != startId) {
                    System.out.print(distances[i]);
                    System.out.print(" ");
                }
            }
            System.out.println();
        }

        scanner.close();
    }
}
