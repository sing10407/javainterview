package DP;

public class LongestPalindromicSubstring {


    public static void main(String[] args) {
        System.out.println(longestPalindrome("babad"));
        System.out.println(longestPalindrome("cbbd"));
        System.out.println(longestPalindromeDP("babad"));
        System.out.println(longestPalindromeDP("cbbd"));
    }
    // Niubi solution
    public static String longestPalindrome(String s) {

        String maxPalindrome = "";
        for(int i = 0 ; i < s.length() ; i ++){

            String tmpString1 = getMaxPalindromeFromIndex(s, i, i);
            String tmpString2 = getMaxPalindromeFromIndex(s, i, i+1);

            if(maxPalindrome.length() < tmpString1.length())
                maxPalindrome = tmpString1;
            if(maxPalindrome.length() < tmpString2.length())
                maxPalindrome = tmpString2;

        }
        return maxPalindrome;
    }

    private static String getMaxPalindromeFromIndex(String s, int j, int k) {

        String currentMax = "";
        while(j >= 0 && k < s.length() && s.charAt(j) == s.charAt(k)){
            currentMax = s.substring(j, k + 1);
            j--;
            k++;
        }
        return currentMax;
    }

    public static String longestPalindromeDP(String s) {

        if(s.length() <= 1)
            return s;

        boolean[][] dp = new boolean[s.length() + 1][s.length() + 1];

        String maxPalindrome = "";

        for(int i = 1 ; i < s.length() ; i ++ ){
            for(int j = i ; j >= 0 ; j--){
                if(s.charAt(i) == s.charAt(j)
                        && (i-j <= 2 || dp[j + 1][i - 1]) // i-j <= 2 means string are too short, like aba, but s[0]==s[2]
                ){
                    dp[j][i] = true;
                    String tmp = s.substring(j, i + 1);
                    if(tmp.length() > maxPalindrome.length())
                        maxPalindrome = tmp;
                }
            }
        }
        return maxPalindrome;
    }
}
