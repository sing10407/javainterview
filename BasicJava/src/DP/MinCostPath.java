package DP;

public class MinCostPath {
    public static void main(String[] args) {
        int[][] arr = new int [][]{ { 3, 2, 8 }, { 1, 9, 7 }, { 0, 5, 2 }, {6, 4, 3} };
        System.out.println(getMinCost(arr, 3,2));
        System.out.println(getMinCostDP(arr, 3,2));
    }
    public static int getMinCost(int[][] cost, int m, int n){
        if (n < 0 || m < 0)
            return Integer.MAX_VALUE;
        else if (m == 0 && n == 0)
            return cost[m][n];
        else
            return cost[m][n] +
                    min( getMinCost(cost, m-1, n-1),
                            getMinCost(cost, m-1, n),
                            getMinCost(cost, m, n-1) );
    }
    static int min(int x, int y, int z)
    {
        if (x < y)
            return (x < z) ? x : z;
        else
            return (y < z) ? y : z;
    }
    private static int getMinCostDP(int cost[][], int m, int n)
    {
        /*
        * original matrix
        * | 3   | 2   | 8   |
        * | 1   | 9   | 7   |
        * | 0   | 5   | 2   |
        * | 6   | 4   | 3   |
        *
        * fill min cost matrix first
        * minimumCostPath[i][0] = minimumCostPath[i - 1][0] + costMatrix[i][0], for all values of i > 0
        * minimumCostPath[0][j] = minimumCostPath[0][j - 1] + costMatrix[0][j], for all values of j > 0
        * | 3   | 5   | 13  |
        * | 4   | 0   | 0   |
        * | 4   | 0   | 0   |
        * | 10  | 0   | 0   |
        *
        * use DP to fill the others based on m, n axis
        * | 3   | 5   | 13  |
        * | 4   | 12  | 12  |
        * | 4   | 9   | 11  |
        * | 10  | 8   | 11  |
        *
        * */

        int i, j;
        int dp[][]=new int[m+1][n+1];

        dp[0][0] = cost[0][0];

        /* Initialize first column of total cost(tc) array */
        for (i = 1; i <= m; i++)
            dp[i][0] = dp[i-1][0] + cost[i][0];

        /* Initialize first row of tc array */
        for (j = 1; j <= n; j++)
            dp[0][j] = dp[0][j-1] + cost[0][j];

        /* Construct rest of the tc array */
        for (i = 1; i <= m; i++)
            for (j = 1; j <= n; j++)
                dp[i][j] = min(dp[i-1][j-1], dp[i-1][j], dp[i][j-1])
                        + cost[i][j];

        return dp[m][n];
    }

}
