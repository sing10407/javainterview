package DP;

public class MinimumNumberOfCoinsToMakeChange {
    public static void main(String[] args) {
        System.out.println(getMinChange(new int[]{2, 5, 3}, 7));
        System.out.println(getMinChange2(new int[]{2, 5, 3}, 7));
        System.out.println(getMinChange3(new int[]{2, 5, 3}, 7));
        System.out.println(test(new int[]{2, 5, 3}, 7));

        System.out.println(getMinChange(new int[]{25, 10, 5}, 30));
        System.out.println(getMinChange2(new int[]{25, 10, 5}, 30));
        System.out.println(getMinChange3(new int[]{25, 10, 5}, 30));
        System.out.println(test(new int[]{25, 10, 5}, 30));
    }
    public static int getMinChange(int[] coins, int sum){
        int[][] dp = new int[sum + 1][coins.length + 1];
        return getMinChange(coins, sum, 0, 0, dp);
    }

    // money can't repeat
    public static int getMinChange(int[] coins, int sum, int index, int count, int[][] dp){

        if(sum == 0)
            return count;
        if(sum < 0 || index > coins.length-1)
            return Integer.MAX_VALUE;

        if(dp[sum][index]!=0)
            return dp[sum][index];

        dp[sum][index] = Math.min(
                getMinChange(coins, sum - coins[index], index+1, count+1, dp),
                getMinChange(coins, sum, index+1, count, dp)
        );

        return dp[sum][index];
    }

    // Recursive Solution to find minimum number of coins.
    /*
    * If V == 0, then 0 coins required.
    * If V > 0
    * minCoin(coins[0..m-1], V) = min {1 + minCoins(V-coin[i])}
                               where i varies from 0 to m-1
                               and coin[i] <= V
    * */
    public static int getMinChange2(int[] values, int sum) {
        // base case
        if (sum == 0)
            return 0;

        // Initialize result
        int res = Integer.MAX_VALUE;

        // Try every coin that has smaller value than sum
        for (int i=0; i<values.length; i++)
        {
            if (values[i] <= sum)
            {
                int sub_res = getMinChange2(values, sum-values[i]);

                // Check for INT_MAX to avoid overflow and see if
                // result can minimized
                if (sub_res != Integer.MAX_VALUE && sub_res + 1 < res)
                    res = sub_res + 1;
            }
        }
        return res;
    }

    // DP Solution to find minimum number of coins
    public static int getMinChange3(int[] values, int sum) {

        int[] minCoins = new int[sum + 1];
        for(int i = 1; i <= sum; i++) {
            int min = Integer.MAX_VALUE;
            for(int j = 0; j < values.length; j++) {
                if(i >= values[j])
                    min = Math.min(min, minCoins[i - values[j]]);
            }
            if(min != Integer.MAX_VALUE)
                minCoins[i] = min + 1;
            else
                minCoins[i] = Integer.MAX_VALUE;
        }
        return minCoins[sum];
    }



    public static int test(int[] nums, int sum){

        int[] dp = new int[sum+1];
        for (int i = 1 ; i <= sum ; i ++){
            int min = Integer.MAX_VALUE;
            for(int j = 0 ; j < nums.length ; j++){
                if(i >= nums[j]){
                    min = Math.min(min ,dp[i - nums[j]] + 1);
                }
            }
            if(min != Integer.MAX_VALUE)
                dp[i] = min;
        }
        return dp[sum];
    }

}
