package DP;

public class LongestIncreasingSubsequence {
    public static void main(String[] args) {
        System.out.println(getSubsequence(new int[]{12, 18, 7, 34, 30, 28, 90, 88}));
        System.out.println(getSubsequence(new int[]{3, 10, 2, 1, 20}));
        System.out.println(getSubsequence(new int[]{3, 2}));
        System.out.println(getSubsequence(new int[]{50, 3, 10, 7, 40, 80}));

        System.out.println(getSubsequenceDP(new int[]{12, 18, 7, 34, 30, 28, 90, 88}));
        System.out.println(getSubsequenceDP(new int[]{3, 10, 2, 1, 20}));
        System.out.println(getSubsequenceDP(new int[]{3, 2}));
        System.out.println(getSubsequenceDP(new int[]{50, 3, 10, 7, 40, 80}));
    }

    // brute force
    public static int getSubsequence(int[] arr){

        int longest = 0;
        for(int left = arr.length -1 ; left >= 0 ; left --){
            for(int right = left ; right < arr.length ; right ++){
                int currentCount = 1;
                for(int i = left ; i <= right ; i ++){
                    if(i>0 && arr[i] > arr[i -1]){
                        currentCount++;
                    }
                }
                longest = Math.max(longest, currentCount);
            }
        }
        return longest;
    }

    // DP
    public static int getSubsequenceDP(int[] arr){

        int[] dp = new int[arr.length];

        int longest = 0;

        dp[0] = 1;
        for(int i = 1 ; i < arr.length ; i ++){
            dp[i] = 1;

            // notice: j must < i, if used j < i, it would cause bug.
            for(int j = 0 ; j < i ; j ++){
                if(arr[i] > arr[i - 1]){
                    dp[i] = Math.max(dp[i], dp[j] + 1);
                }
            }
            longest = Math.max(longest, dp[i]);
        }

        return longest;
    }
}
