package DP;

import java.util.HashSet;
import java.util.Hashtable;

/*
* https://leetcode.com/problems/word-break/submissions/
* Given a dictionary of words and a string of characters, find if the string of characters can be broken into individual valid words from the dictionary.
* Example:
* Dictionary: arrays, dynamic, heaps, IDeserve, learn, learning, linked, list, platform, programming, stacks, trees
* String    : IDeservelearningplatform
* Output   : true
* Because the string can be broken into valid words: IDeserve learning platform
*
* array of : I D e s e r v e l e a r n i n g p l a t f o r m
* status :                 t         t     t               t
*
* */
public class WordBreakProblem {
    public static void main(String[] args) {
        HashSet<String> wordDictionary = new HashSet<String>(){};
        wordDictionary.add("arrays");
        wordDictionary.add("dynamic");
        wordDictionary.add("heaps");
        wordDictionary.add("IDeserve");
        wordDictionary.add("learn");
        wordDictionary.add("learning");
        wordDictionary.add("linked");
        wordDictionary.add("list");
        wordDictionary.add("platform");
        wordDictionary.add("programming");
        wordDictionary.add("stacks");
        wordDictionary.add("trees");

        String s="IDeservelearningplatform";

        System.out.println(getWordBreakResult(wordDictionary, s));
    }

    private static boolean getWordBreakResult(HashSet<String> wordDict, String s) {

        boolean[] f = new boolean[s.length() + 1];
        int stringLength = s.length();

        for(int i = 0 ; i < stringLength ; i++){
            for(int j = i ; j < stringLength ; j++){
                if(i==0){
                    // case a, ["a"]
                    if(wordDict.contains(s.substring(i, j+1)))
                        f[j] = true;
                // if each starting char is true, the following word would be consecutive
                }else if(f[i-1] && wordDict.contains(s.substring(i, j+1))){
                    f[j] = true;
                }
            }
        }

        for (int i = 0 ; i < stringLength ; i++) {
            System.out.println(i+" "+s.charAt(i) +"  "+f[i]);
        }

        return f[s.length()-1];
    }












    private static boolean getWordBreakResult2(HashSet<String> wordDict, String s) {

        boolean[] f = new boolean[s.length() + 1];

        f[0] = true;


        /* First DP
        for(int i = 1; i <= s.length(); i++){
            for(String str: dict){
                if(str.length() <= i){
                    if(f[i - str.length()]){
                        if(s.substring(i-str.length(), i).equals(str)){
                            f[i] = true;
                            break;
                        }
                    }
                }
            }
        }*/

        //Second DP
        for(int i=1; i <= s.length(); i++){
            for(int j=0; j < i; j++){
                if(f[j] && wordDict.contains(s.substring(j, i))){
                    f[i] = true;
                    break;
                }
            }
        }

        return f[s.length()];
    }
}
