package DP;

public class LongestCommonSubsequence {
    public static void main(String[] args) {

        LongestCommonSubsequence lcs = new LongestCommonSubsequence();
        System.out.println(lcs.getString("ACBEA", "ADCA"));
        System.out.println(lcs.getStringDP("ACBEA", "ADCA"));
        System.out.println(lcs.getStringDP2("ACBEA".toCharArray(), "ADCA".toCharArray(), "ACBEA".length(), "ADCA".length()));
    }
    public int getString(String s1, String s2){

        return getString(s1, s2, s1.length() -  1, s2.length() - 1);
    }

    public int getString(String s1, String s2, int indexS1, int indexS2){

        if(indexS1 < 0 || indexS2 < 0){
            return  0;
        }else if(s1.charAt(indexS1) == s2.charAt(indexS2)){
            return 1 + getString(s1, s2, indexS1 -1, indexS2 -1);
        }else{
            return Math.max(
                    getString(s1, s2, indexS1 - 1, indexS2),
                    getString(s1, s2, indexS1, indexS2 - 1)
            );
        }
    }

    public int getStringDP(String s1, String s2){

        int[][] dp = new int[s1.length()][s2.length()];
        return getStringDP(s1, s2, s1.length() -  1, s2.length() - 1, dp);
    }

    public int getStringDP(String s1, String s2, int indexS1, int indexS2, int[][] dp){


        if(indexS1 >=0 && indexS2 >=0 && dp[indexS1][indexS2] != 0)
            return dp[indexS1][indexS2];

        int result = 0;

        if(indexS1 < 0 || indexS2 < 0){
            result = 0;
        }else if(s1.charAt(indexS1) == s2.charAt(indexS2)){
            result = 1 + getString(s1, s2, indexS1 -1, indexS2 -1);
        }else{
            result = Math.max(
                    getString(s1, s2, indexS1 - 1, indexS2),
                    getString(s1, s2, indexS1, indexS2 - 1)
            );
        }
        dp[indexS1][indexS2] = result;
        return result;
    }
    int getStringDP2( char[] X, char[] Y, int m, int n )
    {
        int L[][] = new int[m+1][n+1];

        /* Following steps build L[m+1][n+1] in bottom up fashion. Note
         that L[i][j] contains length of LCS of X[0..i-1] and Y[0..j-1] */
        for (int i=0; i<=m; i++)
        {
            for (int j=0; j<=n; j++)
            {
                if (i == 0 || j == 0)
                    L[i][j] = 0;
                else if (X[i-1] == Y[j-1])
                    L[i][j] = L[i-1][j-1] + 1;
                else
                    L[i][j] = Math.max(L[i-1][j], L[i][j-1]);
            }
        }
        return L[m][n];
    }
}
