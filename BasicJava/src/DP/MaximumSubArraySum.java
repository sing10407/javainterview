package DP;



/*
* Given an array of unordered positive and negative integers, find the maximum subarray sum in the array.
* For example:
* Array: {2, -9, 5, 1, -4, 6, 0, -7, 8}
* Output:
* Maximum subarray sum is 9
*
* */
public class MaximumSubArraySum {
    public static void main(String[] args) {
        int[] arr = new int[]{2, -9, 5, 1, -4, 6, 0, -7, 8};
        System.out.println(getMaxiumSubArraySum(arr));
        System.out.println(bruteForce(arr));

        int[] arr2 = new int[]{-2, -9, -5, -1, -4, -6, -7, -8};
        System.out.println(getMaxiumSubArraySum(arr2));
        System.out.println(bruteForce(arr2));

    }

    private static int getMaxiumSubArraySum(int[] arr) {
        int curruntSum=0, maxSum=0;

        boolean isAllNegative = true;
        int maxValue=Integer.MIN_VALUE;
        for(int i : arr) {
            if (i > 0)
                isAllNegative = false;
            if(i>maxValue)
                maxValue = i;
        }

        if(isAllNegative)
            return maxValue;

        // Main logic
        // use Kadane algorithm
        for(int i=0;i<arr.length;i++){
            curruntSum += arr[i];

            if(curruntSum<0)
                curruntSum = 0;

            if(curruntSum>maxSum)
                maxSum = curruntSum;
        }
        return maxSum;
    }

    private static int bruteForce(int[] arr){
        int max=Integer.MIN_VALUE;

        boolean isAllNegative = true;
        int maxValue=Integer.MIN_VALUE;
        for(int i : arr) {
            if (i > 0)
                isAllNegative = false;
            if(i>maxValue)
                maxValue = i;
        }

        if(isAllNegative)
            return maxValue;

        for (int start = 0;start<arr.length;start++){
            for(int end =0;end<arr.length;end++){
                int sum =0;
                for(int i=start;i<=end;i++){
                    sum += arr[i];
                }
                if(max<sum)
                    max=sum;
            }
        }
        return max;
    }
}
