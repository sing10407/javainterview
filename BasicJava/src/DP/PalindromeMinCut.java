package DP;

public class PalindromeMinCut {
    public static void main(String[] args) {
        System.out.println(minCut("aab"));
        System.out.println(minCut("ababbbabbababa"));// aba,b,bbabb,ababa
    }

    public static int minCut(String s){
        int sLength = s.length();

        // cut[i] = min cut from 0 ~ i
        int[] cut = new int[sLength+1];
        // dp[i][j] is palindrome or not
        boolean[][] dp = new boolean[sLength+1][sLength+1];

        /*
        * cut[i] is  min(cut[j - 1] + 1), if [j, i] is palindrome.    (j <= i)
        * */
        for(int i=0;i<sLength;i++){
            // abcdef  if i=3(abcd)
            // a b c d worst case is 3
            int min = i;
            for(int j=0;j<=i;j++){
                // If [j, i] is palindrome   (j<=i)
                // then cut[i] = cut[j-1] + 1(= [j, i] = 1 palindrome)
                if(isPalindrome(i, j, dp, s)){
                    dp[i][j] = true;
                    if(j==0)
                        min = 0; // if j~i is Palindrome and j==0, mean 0~i is Palindrome, no needs to cut
                    else
                        min = Math.min(min, cut[j-1]+1); // min(dp[i], dp[j - 1 ] + 1 )
                }
            }
            cut[i] = min;
        }
        return cut[sLength-1];
    }

    public static boolean isPalindrome(int i, int j, boolean[][] dp, String s){
        // if [j, i] is palindrome, [j + 1, i - 1] is palindrome, and c[j] == c[i].
        return s.charAt(i) == s.charAt(j) && ((j+1 > i-1) || dp[i-1][j+1]);
    }
}
