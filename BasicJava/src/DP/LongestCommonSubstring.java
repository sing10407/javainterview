package DP;

import java.util.ArrayList;

public class LongestCommonSubstring {

    public static void main(String[] args) {

        System.out.println(getString("LCLC","CLCL"));
        System.out.println(getString2("LCLC","CLCL"));
    }
    public static ArrayList<String> getString(String S1, String S2){

        Integer match[][] = new Integer[S1.length()][S2.length()];

        int len1 = S1.length();
        int len2 = S2.length();
        int max = Integer.MIN_VALUE; //Maximum length of the string
        ArrayList<String> result = null; //Result list

        for(int i=0; i<len1; i++)
        {
            for(int j=0; j<len2; j++)
            {
                if(S1.charAt(i) ==  S2.charAt(j))
                {
                    if ( i == 0 || j==0)
                        match[i][j] = 1;
                    else
                        match[i][j] = match[i-1][j-1] + 1;


                    if(match[i][j] > max) //If you find a longer common substring re-initialize the max count and update the result list.
                    {
                        max =  match[i][j];
                        result = new ArrayList<String>();
                        result.add(S1.substring(i-max+1, i+1)); //substring starts at i-max+1 and ends at i
                    }
                    else if(match[i][j] == max) // else if you find a common substring with the max length, store it in the list.
                    {
                        result.add(S1.substring(i-max+1, i+1));
                    }
                }
                else{
                    match[i][j] = 0;
                }
            }
        }
        return result;
    }


    public static ArrayList<String> getString2(String s1, String s2){

        ArrayList<String> result = new ArrayList<String>();
        int[][] dp = new int[s1.length()][s2.length()];
        int maxLength = Integer.MIN_VALUE;

        for(int i = 0 ; i < s1.length() ; i ++){
            for(int j = 0 ; j < s2.length() ; j ++){
                if(s1.charAt(i) == s2.charAt(j)){
                    if(i == 0 || j == 0){
                        dp[i][j] = 1;
                    }else{
                        dp[i][j] = dp[i -1][j - 1] + 1;
                    }
                    if(dp[i][j] > maxLength){
                        maxLength = dp[i][j];
                        result.clear();
                        result.add(s1.substring(i - dp[i][j] + 1, i + 1));
                    }else if(dp[i][j] == maxLength){
                        result.add(s1.substring(i - dp[i][j] + 1, i + 1));
                    }
                }
            }
        }
        return result;
    }

}
