package DP;


import java.util.Hashtable;

/*
* https://www.youtube.com/watch?v=nqlNzOcnCfs&t=841s
*
*
* */
public class SubsetSumProblem {
    public static void main(String[] args) {
        int[] arr = new int[]{3, 34, 4, 12, 5, 2};
        System.out.println(SubsetSum(arr, 9));
        System.out.println(SubsetSumDP(arr, 9));
    }

    public static int SubsetSum(int[] arr, int total){
        return SubsetSum(arr, total, arr.length-1);
    }

    public static int SubsetSum(int[] arr, int total, int i){

        if (total == 0)
            return 1;
        else if (i<0)
            return 0;
        else if (total < arr[i]) // will exceed sum, so skip to next item
            return SubsetSum(arr, total, i-1);
        else{
            return SubsetSum(arr, total-arr[i], i-1) + SubsetSum(arr, total, i-1);
        }
    }

    public static int SubsetSumDP(int[] arr, int total){
        Hashtable<String, Integer> hashtable = new Hashtable<String, Integer>();
        return SubsetSumDP(arr, total, arr.length-1, hashtable);
    }

    public static int SubsetSumDP(int[] arr, int total, int i, Hashtable hashtable){
        String key = total + ":" + i ;
        if(hashtable.contains(key))
            return (int)hashtable.get(key);

        int result = 0;

        if (total == 0)
            return 1;
        else if (i<0)
            return 0;
        else if (total < arr[i])
            result = SubsetSum(arr, total, i-1);
        else
            result = SubsetSum(arr, total-arr[i], i-1) + SubsetSum(arr, total, i-1);

        hashtable.put(key, result);
        return result;
    }

}
