package DP;

public class LongestPalindromicSubsequence {
    public static void main(String[] args) {

        System.out.println(getSubsequence("bbbbb"));
        System.out.println(getSubsequence("bbbab"));
        System.out.println(getSubsequence("cbbd"));
    }
    public static int getSubsequence(String s){
        int[][] dp = new int[s.length()][s.length()];

        for(int left = s.length()-1 ; left >=0 ; left --){
            dp[left][left] = 1;
            // we already know every dp[i][i], so right index start from left + 1
            for(int right = left + 1 ; right < s.length() ; right ++ ){
                if(s.charAt(left) == s.charAt(right)){
                    // edge case bbbbb
                    // then here would be dp[3][4] = dp[4][3] + 2
                    // but dp[4][3] no any meaning, but it's ok, the value is 0, and no out of array bounds.
                    dp[left][right] = dp[left + 1][right - 1] + 2;
                }else{
                    dp[left][right] = Math.max(dp[left + 1 ][right], dp[left][right - 1]);
                }
            }
        }
        return dp[0][s.length()-1];
    }
}
