package DP;

public class TotalNumberOfPossibleBinarySearchTreesWithNKeys {
    public static void main(String[] args) {
        int num = 5;
        System.out.println(getNKeys(num));

        int[] dp = new int[num+1];
        System.out.println(getNKeysDP(num, dp));
    }
    public static int getNKeys(int num){

        if(num<=1)
            return 1;

        int sum =0;

        for(int i=1;i<=num;i++){
            int leftTreeCount = getNKeys(i-1);
            int rightTreeCount = getNKeys(num-i);

            sum += leftTreeCount * rightTreeCount;
        }
        return sum;
    }

    public static int getNKeysDP(int num, int[] dp){

        if(num<=1)
            return 1;

        int sum =0;
        for(int i=1;i<=num;i++){
            if(dp[i-1]==0)
                dp[i-1] = getNKeysDP(i-1, dp);

            if(dp[num-i]==0)
                dp[num-i] = getNKeysDP(num-i, dp);

            sum += dp[i-1] * dp[num-i];
        }
        return sum;
    }
}
