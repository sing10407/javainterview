package DP;

// Shortest Palindrome
// Example:
// abab => babab
// abcd => dcbabcd
// ananab => bananab.

// algorithm
// abcd
// dcba
// dcba abcd
public class ShortestPalindrome {
    public static void main(String[] args) {
        System.out.println(ShortestPalindrome("abab"));
        System.out.println(ShortestPalindrome("abcd"));
        System.out.println(ShortestPalindrome("ananab"));

        System.out.println(ShortestPalindromeKMP("abab"));
        System.out.println(ShortestPalindromeKMP("abcd"));
        System.out.println(ShortestPalindromeKMP("ananab"));
    }
    public static String ShortestPalindrome(String s){
        if(isPalindrome(s))
            return s;

        String reverseds =  new StringBuilder(s).reverse().toString();
        int start = 0;
        int end = s.length()-1;
        String lastPalindrome ="";

        while(end>=0){
            if(s.charAt(start) == reverseds.charAt(end)){
                String currentString = reverseds.substring(0, end+1) + s;
                if(isPalindrome(currentString)) {
                    lastPalindrome = currentString;
                }
                start++;
                end--;
            }
        }

        return lastPalindrome;
    }
    public static boolean isPalindrome(String s){
        int start=0, end=s.length()-1;
        while (start<end){
            if(s.charAt(start) != s.charAt(end))
                return false;
            start++;
            end--;
        }
        return true;
    }


    public  static String ShortestPalindromeKMP(String s)
    {
        String rev_s = new StringBuilder(s).reverse().toString();
        // use special character to avoid overlap
        // abcd#dcba
        String l = s + "#" + rev_s;

        int[] p = new int[l.length()];

        // build KMP table
        // how: https://youtu.be/GTJr8OvyEVQ?t=339
        for(int i=1; i<l.length(); i++)
        {
            //update prefix boundary to previous match position
            int j = p[i-1];

            //move to the last prefix boundary match
            while(j>0 && l.charAt(i)!=l.charAt(j))
                j = p[j-1];

            //if prefix boundary matches suffix boundary,
            //increase prefix length
            if(l.charAt(i) == l.charAt(j))
                p[i] = j + 1;
        }

        return rev_s.substring(0, s.length() - p[l.length() - 1]) + s;
    }

}
