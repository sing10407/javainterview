package DataStructure;

import java.util.Hashtable;

public class DisjointSet {

    class Node{
        Node parent;
        Long data;
        int rank;
    }

    private Hashtable<Long, Node> dset = new Hashtable<>();

    public void makeSet(long data){
        Node newNode = new Node();
        newNode.parent = newNode;
        newNode.rank = 0;
        newNode.data = data;
        dset.put(data, newNode);
    }
    public boolean union(long a, long b){
        Node aNode = dset.get(a);
        Node bNode = dset.get(b);

        Node aRoot = findSet(aNode);
        Node bRoot = findSet(bNode);

        if(aRoot == bRoot){
            return false;
        }else{
            if(aRoot.rank>=bRoot.rank){
                bRoot.parent = aRoot;
                aRoot.rank = (aRoot.rank > bRoot.rank) ? aRoot.rank : bRoot.rank+1;
            }else{
                aRoot.parent=bRoot;
            }
            return true;
        }

    }
    public Node findSet(Node data){
        if(data.parent == data)
            return data;
        Node result = findSet(data.parent);
        return result;
    }

    public long findSet(long data){
        Node result = dset.get(data);
        return result.data;
    }

    public static void main(String[] argv){
        DisjointSet ds = new DisjointSet();
        ds.makeSet(1);
        ds.makeSet(2);
        ds.makeSet(3);
        ds.makeSet(4);
        ds.makeSet(5);
        ds.makeSet(6);
        ds.makeSet(7);

        ds.union(1, 2);
        ds.union(2, 3);
        ds.union(4, 5);
        ds.union(6, 7);
        ds.union(5, 6);
        ds.union(3, 7);

        System.out.println(ds.findSet(1));
        System.out.println(ds.findSet(2));
        System.out.println(ds.findSet(3));
        System.out.println(ds.findSet(4));
        System.out.println(ds.findSet(5));
        System.out.println(ds.findSet(6));
        System.out.println(ds.findSet(7));
    }

}
