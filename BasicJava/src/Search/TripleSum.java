package Search;

import java.util.Arrays;

// https://www.hackerrank.com/challenges/triple-sum
public class TripleSum {
    // Complete the triplets function below.
    static long triplets(int[] a, int[] b, int[] c) {

        Arrays.sort(a);
        a=Arrays.stream(a).distinct().toArray();
        Arrays.sort(b);
        b=Arrays.stream(b).distinct().toArray();
        Arrays.sort(c);
        c= Arrays.stream(c).distinct().toArray();
        int aIndex = 0;
        int bIndex = 0;
        int cIndex = 0;
        long result = 0;

        while(bIndex<b.length){
            while(aIndex<a.length && a[aIndex]<=b[bIndex])
                aIndex++;

            while(cIndex<c.length && c[cIndex]<=b[bIndex])
                cIndex++;

            // just need to multiply, increasing performance
            // no needs to list all results
            result += ((long)aIndex)*((long)cIndex);
            bIndex++;
        }
        return result;
    }
}
