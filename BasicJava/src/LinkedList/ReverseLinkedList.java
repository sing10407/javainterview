package LinkedList;

import java.util.Stack;

public class ReverseLinkedList {
    static class DoublyLinkedListNode {
        public int data;
        public DoublyLinkedListNode next;
        public DoublyLinkedListNode prev;

        public DoublyLinkedListNode(int nodeData) {
            this.data = nodeData;
            this.next = null;
            this.prev = null;
        }
    }

    static DoublyLinkedListNode reverseByStack(DoublyLinkedListNode head) {

        Stack<DoublyLinkedListNode> stack = new Stack<DoublyLinkedListNode>();
        stack.push(head);

        while(head.next != null){
            stack.push(head.next);
            head = head.next;
        }

        DoublyLinkedListNode newHead = stack.pop();
        DoublyLinkedListNode originalHead = newHead;
        while(!stack.empty()){
            DoublyLinkedListNode temp = stack.pop();
            newHead.next = temp;
            temp.prev = newHead;

            newHead = newHead.next;
        }
        newHead.next = null;
        return originalHead;
    }

    static DoublyLinkedListNode ReverseNonRecursive(DoublyLinkedListNode head) {
        while(head.next != null){
            DoublyLinkedListNode tmp = head.next;
            head.next = head.prev;
            head.prev = tmp;
            head = tmp; // moving to next
        }
        head.next = head.prev;
        head.prev = head;
        return head;
    }

    static DoublyLinkedListNode reverseRecursive(DoublyLinkedListNode curr) {
        DoublyLinkedListNode temp = curr.next;
        curr.next = curr.prev;
        curr.prev = temp;
        return temp == null ? curr : reverseRecursive(temp);
    }
}
