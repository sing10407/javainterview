package LeetCode;

// https://leetcode.com/problems/rectangle-area/
public class RectangleArea {
    public int computeArea(int A, int B, int C, int D, int E, int F, int G, int H) {
        int result = Math.abs((C-A)*(D-B))+Math.abs((G-E)*(H-F));

        int overlap = 0;

        if(!(E>C || D<F || A>G || B>H)){
            overlap = Math.abs((Math.min(C, G)-Math.max(A, E)) * (Math.min(D, H)-Math.max(B, F)));
        }
        return result - overlap;
    }

}
